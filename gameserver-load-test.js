import { sleep } from 'k6';
import { SharedArray } from 'k6/data';
import { vu } from 'k6/execution';
import ws from 'k6/ws';
import { randomIntBetween } from 'https://jslib.k6.io/k6-utils/1.1.0/index.js';
import http from 'k6/http';


const connections = new SharedArray('connections', function () {
  return JSON.parse(open('addresses.json'));
});


export const options = {
  scenarios: {
    sending_messages: {
      executor: 'per-vu-iterations',
      vus: connections.length,
      iterations: 1,
      maxDuration: '10m',
      gracefulStop: '5s',
    },
  },
};


export default function () {
    // VU identifiers are one-based and arrays are zero-based, thus we need - 1
    //const url = 'ws://0.0.0.0:5000/chat';
    const url = 'ws://' + connections[vu.idInTest - 1]['address'] + ':' + connections[vu.idInTest - 1]['port'] + '/chat';
    const params = { };

    //console.log(connections[vu.idInTest - 1]);
    const roomId = connections[vu.idInTest - 1]['roomId'];
    console.log(roomId);
    const res = ws.connect(url, params, function (socket) {
        socket.on('open', () => {
            console.log('connected') 
            socket.send(createSignalRMessage({
                "protocol": "json",
                "version": 1
            }));

            socket.send(createSignalRMessage({
            "type": 1,
            "target": "JoinRoom",
            "arguments": [
                `${roomId}`,
                `${vu.idInTest - 1}`
            ]
            }));

            (function loop() {
                var rand = randomIntBetween(800, 1200);
                socket.setTimeout(function() {
                    socket.ping();
                    socket.send(createSignalRMessage({
                        "type": 1,
                        "target": "Send",
                        "arguments": [
                            `${roomId}`,
                            `${vu.idInTest - 1}`,
                            "Nanu Nanu"
                        ]
                    })); 
                    loop();
                }, rand)
            }());  
        });

        socket.on('message', (data) => {
            //console.log(data)
        });
        socket.on('close', () => console.log('disconnected'));
        socket.on('error', function (e) {
            if (e.error() != 'websocket: close sent') {
            console.log('An unexpected error occured: ', e.error());
            }
        });
    });
}


function createSignalRMessage(json) {
    return JSON.stringify(json) + String.fromCharCode(30);
}