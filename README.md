Load testing for the game servers. 

# Dependencies

- k6 (https://k6.io/docs/getting-started/installation/) (learn about k6: https://www.youtube.com/watch?v=5OgQuVAR14I)

- python3

- python kubernetes client library (`pip3 install kubernetes`)

- have the target cluster as the current kube config

# Steps

1. `python3 get-gameserver-addresses.py`

`addresses.json` will be created

2. `k6 run gameserver-load-test.js`

Run the load test for as long as you'd like, cancel with ctrl-c. 
The test will create 4 websocket connections per gameserver running on the kubernetes cluster, and will send a message on average once per second with some random variation