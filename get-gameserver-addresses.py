from kubernetes import client, config
import json
import sys

CONNECTIONS_PER_SESSION = 4
ROOMS_PER_SERVER = 25

if(len(sys.argv) > 1 and sys.argv[1] == "local"):
    gameservers = [{"status": { "state": "Ready", "address": "0.0.0.0", "ports": [ { "port": 5001 } ] } }]
else: 
    config.load_kube_config()

    api = client.CustomObjectsApi()
    gameservers = api.list_cluster_custom_object(group="agones.dev", version="v1", plural="gameservers")["items"]

address_list = []

count = 0 

for gameserver in gameservers:

    if(gameserver["status"]["state"] == "Ready"):
        count += 1
        for roomId in range(1, ROOMS_PER_SERVER + 1):
            thing = {}
            address = gameserver["status"]["address"]
            port = gameserver["status"]["ports"][0]["port"]
            thing["address"] = address
            thing["port"] = port
            thing["roomId"] = roomId

            for i in range(CONNECTIONS_PER_SESSION):
                address_list.append(thing)

print("There are " + str(count) + " ready game servers in the cluster")

json_obj = json.dumps(address_list, indent=4)

with open("addresses.json", "w") as outfile:
    outfile.write(json_obj)