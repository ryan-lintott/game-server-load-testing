import { sleep } from 'k6';
import { SharedArray } from 'k6/data';
import { vu } from 'k6/execution';
import ws from 'k6/ws';
import { randomIntBetween } from 'https://jslib.k6.io/k6-utils/1.1.0/index.js';
import http from 'k6/http';


const connections = new SharedArray('connections', function () {
  return JSON.parse(open('addresses.json'));
});


export const options = {
  scenarios: {
    creating_rooms: {
      executor: 'per-vu-iterations',
      vus: (connections.length / 4),
      iterations: 1,
      maxDuration: '10s',
      exec: "createRooms",
    },
  },
};

export function createRooms() {
    // VU identifiers are one-based and arrays are zero-based, thus we need - 1
    const index = (vu.idInTest - 1) * 4;
    const url = 'http://' + connections[index]['address'] + ':' + connections[index]['port'] + '/gameserver';
    const params = { };
    const response = http.post(url);
    console.log(response.body);
}

